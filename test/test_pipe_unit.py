from unittest.mock import patch, MagicMock
from pipe.pipe import PyPIPublish


@patch('pipe.pipe.os')
@patch('pipe.pipe.subprocess')
@patch('pipe.pipe.upload')
@patch('pipe.pipe.Settings')
def test_create_dist(mock_settings, mock_upload, mock_subprocess, mock_os):
    mock_subprocess.run.return_value.returncode = 0
    pipe = PyPIPublish(pipe_metadata={}, schema={}, check_for_newer_version=True)
    pipe.fail = MagicMock()

    pipe.create_dist(['sdist'])
    mock_subprocess.run.assert_called_once_with(['python', '-m', 'build', '--sdist'], check=False)
    pipe.fail.assert_not_called()


@patch('pipe.pipe.os')
@patch('pipe.pipe.subprocess')
@patch('pipe.pipe.upload')
@patch('pipe.pipe.Settings')
def test_success_upload(mock_settings, mock_upload, mock_subprocess, mock_os):
    pipe = PyPIPublish(pipe_metadata={}, schema={}, check_for_newer_version=True)
    pipe.get_variable = MagicMock(side_effect=['username', 'password', 'repository', 'sdist wheel', '.', False, 'false'])
    pipe.create_dist = MagicMock()
    pipe.success = MagicMock()

    pipe.run()

    pipe.create_dist.assert_called_once_with(['sdist', 'wheel'])
    mock_settings.assert_called_once_with(username='username', password='password', repository_url='repository', skip_existing=False, verbose=False)
    mock_upload.assert_called_once_with(mock_settings.return_value, ['dist/*', ])
    pipe.success.assert_called_once_with('Successfully uploaded the package to repository')
