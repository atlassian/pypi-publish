FROM python:3.11-slim

# install requirements
COPY requirements.txt /
WORKDIR /
RUN pip install --no-cache-dir -r requirements.txt

# copy the pipe source code
COPY LICENSE.txt README.md pipe.yml /
COPY pipe /

ENTRYPOINT ["python3", "/pipe.py"]
