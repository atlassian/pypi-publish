import os
import subprocess

import yaml
from twine.commands.upload import upload
from twine.settings import Settings

from bitbucket_pipes_toolkit import Pipe, get_logger

schema = {
    "PYPI_USERNAME": {"required": True, "type": "string"},
    "PYPI_PASSWORD": {"required": True, "type": "string"},
    "REPOSITORY": {"required": False, "default": "https://upload.pypi.org/legacy/"},
    "DISTRIBUTIONS": {"required": False, "default": "sdist"},
    "FOLDER": {"required": False, "default": "."},
    "SKIP_EXISTING": {"required": False, "default": False},
    "DEBUG": {"required": False, "default": "false"}
}

logger = get_logger()


class PyPIPublish(Pipe):
    initial_dir = os.getcwd()

    def create_dist(self, distributions):
        distributions_args = [f"--{d}" for d in distributions]
        args = ['python', '-m', 'build'] + distributions_args

        logger.info(f'Building distributions: {distributions}')

        result = subprocess.run(args, check=False)

        if result.returncode != 0:
            self.fail('Failed to build distributions')
        else:
            logger.info('Successfully built distributions')

    def run(self):
        username = self.get_variable('PYPI_USERNAME')
        password = self.get_variable('PYPI_PASSWORD')
        repository = self.get_variable('REPOSITORY')
        distributions = self.get_variable('DISTRIBUTIONS').split()
        folder = self.get_variable('FOLDER')
        skip_existing = self.get_variable('SKIP_EXISTING')
        debug = self.get_variable('DEBUG')

        os.chdir(folder)

        self.create_dist(distributions)

        conf = Settings(username=username,
                        password=password,
                        repository_url=repository,
                        skip_existing=skip_existing,
                        verbose=debug == 'true')

        logger.info(f'Uploading distributions to {repository}')

        upload(conf, ['dist/*', ])

        os.chdir(self.initial_dir)
        self.success(f'Successfully uploaded the package to {repository}')


if __name__ == '__main__':
    with open('/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
    pipe = PyPIPublish(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()
