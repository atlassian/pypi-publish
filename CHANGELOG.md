# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.0.0

- major: Add support for pyproject.toml package configuration.
- minor: Upgrade pipe's base docker image to python:3.11-slim.
- patch: Internal maintenance: bump project's dependencies to the latest.

## 0.9.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 0.9.0

- minor: Bump twine -> zipp to fix vulnerabilities.

## 0.8.0

- minor: Bump cryptography package version to fix vulnerabilities.
- patch: Internal maintenance: Bump pipes versions in pipelines config file.

## 0.7.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.6.2

- patch: Internal maintainance: Refactor tests.

## 0.6.1

- patch: Internal maintenance: Bump pipes dependencies.

## 0.6.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.5.0

- minor: Internal maintenance: Update bitbucket-pipes-toolkit.

## 0.4.0

- minor: Internal maintenance: Bump version of the base Docker image to python:3.10-slim.
- patch: Internal maintenance: Bump version of the bitbucket-pipe-release.
- patch: Internal maintenance: Bump version of the pipe's dependencies.
- patch: Internal maintenance: Update the community link.

## 0.3.1

- patch: Internal maintenance: add bitbucket-pipe-release.

## 0.3.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 0.2.14

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.2.13

- patch: Internal maintenance: Add gitignore secrets.

## 0.2.12

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 0.2.11

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.2.10

- patch: Add warning message about new version of the pipe available.

## 0.2.9

- patch: Added code style checks

## 0.2.8

- patch: Internal maintenance: update pipes toolkit version.

## 0.2.7

- patch: Add git and hg binaries to build container

## 0.2.6

- patch: Minor documentation updates

## 0.2.5

- patch: Fixed the issue with passing multiple DISTRIBUTIONS to the pipe

## 0.2.4

- patch: Updated contributing guidelines

## 0.2.3

- patch: Fix colourised logging.

## 0.2.2

- patch: Fix the default PyPI URL

## 0.2.1

- patch: Fixed the default PyPI URL

## 0.2.0

- minor: Fix docker image name in the release process

## 0.1.0

- minor: Initial release
- minor: Initial release of Bitbucket Pipelines for publishing to PyPI
